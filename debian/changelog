docbook5-xml (5.0-4+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Mar 2025 16:30:04 +0000

docbook5-xml (5.0-4) unstable; urgency=medium

  * QA upload.
  * Orphan the package, see #802377.
  * Mark docbook5-xml Multi-Arch: foreign.
  * Declare compliance with Debian policy 4.7.0.

 -- Helmut Grohne <helmut@subdivi.de>  Mon, 05 Aug 2024 21:29:52 +0200

docbook5-xml (5.0-3apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 16:50:30 +0000

docbook5-xml (5.0-3) unstable; urgency=medium

  * debian/control:
    + Drop Daniel Leidert from the uploaders list since he submitted
      the bug to orphan this package.
    + Add myself into the uploaders list but keep the package to be
      orphaned.
    + Bump debhelper compat to v13.
    + Bump Standards-Version to 4.5.0.
    + Update Vcs-* fields to use git packaging repo under Salsa Debian
      group.
  * debian/rules: Rewrite using dh sequencer.
  * debian/source/format: Convert to "3.0 (quilt)" format.
  * debian-catalog.xml: Move from docbook-5.0/ to debian/ to prevent
    unexpected modification on upstream source code.
  * debian/uscan: Fix following upstream new pattern.

 -- Boyuan Yang <byang@debian.org>  Tue, 02 Jun 2020 23:06:39 -0400

docbook5-xml (5.0-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sun, 14 Feb 2021 07:49:37 +0000

docbook5-xml (5.0-2) unstable; urgency=low

  * docbook-5.0/debian-catalog.xml: Added entry for db4-upgrade.xsl.
  * debian/docbook5-xml.install: Install db4-upgrade.xsl stylesheet into
    stylesheet path (closes: #550711).

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Mon, 12 Oct 2009 15:17:10 +0200

docbook5-xml (5.0-1) unstable; urgency=low

  * Initial release (closes: #490716, #531669).

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Fri, 11 Sep 2009 15:23:39 +0200
